#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QScxmlStateMachine>
#include "statemachine.h"
#include "statemachineclass.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterUncreatableType<QScxmlStateMachine>("StateMachine",
                                                   1, 0,
                                                   "StateMachine",
                                                   QLatin1String("MyStateMachine is not creatable."));

    qmlRegisterType<StateMachineClass>("StateMachineClass",
                                                   1, 0,
                                                   "StateMachineClass");


    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
