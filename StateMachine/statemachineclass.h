#ifndef STATEMACHINECLASS_H
#define STATEMACHINECLASS_H

#include <QObject>
#include <QDebug>
#include "statemachine.h"
#include <QScxmlEvent>

class StateMachineClass : public QObject
{
    Q_OBJECT
public:
    explicit StateMachineClass(QObject *parent = nullptr);

    Q_INVOKABLE void i_vSubmitEvent();

signals:

public slots:

private:
    StateMachine_Test m_tlsmObj;
};

#endif // STATEMACHINECLASS_H
