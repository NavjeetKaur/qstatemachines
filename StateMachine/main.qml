import QtQuick 2.13
import QtQuick.Window 2.12
import QtScxml 5.8

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Name{
        StateMachineLoader {
            id: loader
            source: "qrc:/statemachine.scxml"
        }

        stateMachine: loader.stateMachine
    }
}
