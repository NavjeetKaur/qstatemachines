import QtQuick 2.5

Image {
    id: button
    signal clicked

    MouseArea {
        id: mouse
        anchors.fill: parent
        onClicked: button.clicked()
    }
}
