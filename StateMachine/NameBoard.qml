import QtQuick 2.0
import StateMachine 1.0
import StateMachineClass 1.0

Item {

    property StateMachine stateMachine
    //property StateMachine stateMachine
    property alias button: button


    Button {
        id: button

        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 20
        source: "qrc:/pause.png"
    }

    Column {
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: egg_id
            opacity: stateMachine.Egg ? 1 : 0.2
            text: "EGG"
            color: "black"
        }

        Text {
            id: chicken_id
            opacity: stateMachine.Chicken ? 1 : 0.2
            text: "CHICKEN"
            color: "black"
        }

        Text {
            id: hen_id
            opacity: stateMachine.Hen ? 1 : 0.2
            text: "HEN"
            color: "black"
        }

        Text {
            id: eggFried_id
            opacity: stateMachine.EggFried ? 1 : 0.2
            text: "EggFried"
            color: "black"
        }

        Text {
            id: chickenMeal_id
            opacity: stateMachine.ChickenMeal ? 1 : 0.2
            text: "ChickenMeal"
            color: "black"
        }

        Text {
            id: dead_id
            opacity: stateMachine.Dead ? 1 : 0.2
            text: "Dead"
            color: "black"
        }
    }

//    states: [
//        State {
//            name: "1"
//            when: stateMachine.Egg

//            PropertyChanges {
//                target: egg_id
//                opacity: 1
//            }
//        },
//        State {
//            name: "2"
//            when: stateMachine.Chicken

//            PropertyChanges {
//                target: chicken_id
//                opacity: 1
//            }
//        },
//        State {
//            name: "3"
//            when: stateMachine.Hen

//            PropertyChanges {
//                target: hen_id
//                opacity: 1
//            }
//        },
//        State {
//            name: "4"
//            when: stateMachine.EggFried

//            PropertyChanges {
//                target: eggFried_id
//                opacity: 1
//            }
//        },
//        State {
//            name: "5"
//            when: stateMachine.ChickenMeal

//            PropertyChanges {
//                target: chickenMeal_id
//                opacity: 1
//            }
//        },
//        State {
//            name: "6"
//            when: stateMachine.Dead

//            PropertyChanges {
//                target: dead_id
//                opacity: 1
//            }
//        }
//    ]

}
